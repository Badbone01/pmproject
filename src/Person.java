public class Person {

    private String firstName;
    private String lastName;
    private String city;
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public Person(String firstName, String lastName, String city, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.email = email;
    }
}
